/*
Faculdade: UNIPÊ / ADS
Alunos: Samuel de Oliveira Veloso;
        Samuel Luciano dos Santos Souza.

                                       Snake Game

*/

//Aqui ficam as bibliotecas
#include<windows.h>//Todas as definições de janelas: criar, abrir, etc.
#include<stdio.h>// Essa biblioteca é responsável pela entrada e saída
#include<conio.h>//serve para que vc possa usar em seus programas funções de leitura/escrita (input/output ou IO no meu caso o kgotoxy e getch
#include <stdlib.h>//Ela é responsável pela conversões de números, as alocações nas memórias e outras funções
#include <time.h>//Esta biblioteca declara funções para manipularmos a data e hora


void kgotoxy(int x, int y)// Aqui serve para que o kgotoxy salte de posição para um ponto especifico ou ande na tela
{
 SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),(COORD){x,y});// Aqui é a parte que serve para fazer o cursor obedescer o kgotoxy
}

main()
// Aqui as variaveis
{
// interface (menu)
    int opccao ;
    system("cls");
    printf("\n--------------------------\n");
    printf("\tSnake: the game\n");
    printf("--------------------------\n");
    printf("\n1.Jogar\n2.Sair\n3.Creditos\n ");
    scanf("%d", &opccao);

    switch(opccao){
    case 1:
    system("cls");
    printf("\n\n\n\nCarregando o jogo...");
    Sleep(2000);
    system("cls");
    break;
    case 2:
    exit(0);
    break;
    case 3:
    system("cls");
    printf (" \n\n\tDesenvolvido por Samuel Veloso, Samuel Luciano\n\n");
    int voltar;
    printf("Digite 1 para voltar\n");
    scanf("%d", &voltar);
    if(voltar == 1){
        main();
    }else{
    printf("\nValor incorreto, finalizando programa...");
    Sleep(1500);
    exit(0);
    }
    default:
        system("cls");
        printf("\n\nValor incorreto, programa fianlizado\n\n");
        exit(0);

    }
  //swich(opccao){

 // }
 int x,d=2,cx[300]={1,2},cy[300]={7,7},t=1,mx,my,velo=100,velo2=5;
 char niv;

 char tecla='a';
 int opcao;
 int pontos=0;
 int nivel = 1;//Aqui é o nivel começando do 1


 //Aqui o for para trabalhar a parte grafica cursor e bordas
 for(x=0;x<18;x++)//aqui 18 é o tamanho da borda
 { kgotoxy(0,x); //vertical esquerda.//

 printf("%c",178);//aqui 178  é a borda representada na tabela ascii
 }
 for(x=0;x<50;x++)//aqui 50 é o tamanho da borda
 { kgotoxy(x,0); //horizontal ssuperior//
 printf("%c",178);//aqui 178 é a borda representada na tabela ascii
 }
 for(x=0;x<18;x++)//aqui 18 é o tamanho da borda
 { kgotoxy(50,x); //vertical direita//
 printf("%c",178);//aqui 178 é a borda representada na tabela ascii
 }
 for(x=0;x<51;x++)//aqui 51 é o tamanho da borda
 { kgotoxy(x,18); //horizontal inferior.//
 printf("%c",178);//aqui 178 é a borda representada na tabela ascii
 }




 srand(time(NULL));//Aqui o srand gera a sequencia de valores
 mx=(rand()%49)+1;
 my=(rand()%17)+1;

 velo = 200;//Aqui é a velocidade em que o cursor irá se movimentar

 while(tecla!='s')
 { while(tecla!='s'&&!(tecla=kbhit()))

 { for(x=t;x>0;x--)//x e igual t x e maior que zero e x incrementa menos m
 { cx[x]=cx[x-1];
 cy[x]=cy[x-1];
 }

 if(d==0)cx[0]--;
 if(d==1)cy[0]--;
 if(d==2)cx[0]++;
 if(d==3)cy[0]++;
 kgotoxy(cx[t],cy[t]);
 printf(" ");
 if(mx==cx[0]&&my==cy[0])
 { t++;
 pontos++;
 mx=(rand()%25)+1;
 my=(rand()%17)+1;
 velo-=5;// aqui a velocidade 5 é a máxima
 velo2+=5;// aqui vai da 2 e incrementa até a 5

 }
 kgotoxy(cx[0],cy[0]);

 printf("%c",254);// este aqui é o cursosr que vai andar pela tela que é um quadradinho representado pela tabela ascii

 kgotoxy(mx,my);
 printf("%c",1);// aqui Start of Heading - SOH consultar tabela asciii
 kgotoxy(55,10);// Aqui posicionamento x, y

 printf ("Pontos: %d",pontos);
 kgotoxy(55,5);
 printf ("Nivel: %d",nivel);
 kgotoxy(55,3);
 printf ("Velocidade: %d",velo2);
 kgotoxy(3,22);


 Sleep(velo);
 for(x=1;x<t;x++)
 { if(cx[0]==cx[x]&&cy[0]==cy[x])tecla='s';
 }
 if(cy[0]==0||cy[0]==18||cx[0]==0||cx[0]==50)tecla='s';




 }
 if(tecla!='s')tecla=getch();
 if(tecla=='K')d=0;
 if(tecla=='H')d=1;
 if(tecla=='M')d=2;
 if(tecla=='P')d=3;
 if(cy[0]==0||cy[0]==18||cx[0]==0||cx[0]==26)tecla='s';

 }
 system("cls");
 system("pause");

 printf ("\n\n\tVoce Perdeu!!!\n\n");//Exibir quando perde
 printf ("\n\n\tVoce Fez> %d PONTOS",pontos);// AQUI EXIBE A PONTUAÇÃO

//condições da finalização
 Sleep(2000);
 system("cls");
 int continuar;
 printf("\nDeseja Continuar ?");
 printf("\n1. Continuar\n2. Sair\n");
 scanf("%d", &continuar);

 if(continuar == 1){
    main();
 }else if ( continuar == 2){
    exit(0);
 }else{
    printf("Valor incorreto, finalizando programa....");
    Sleep(1500);
    exit(0);
 }






 getch();
}